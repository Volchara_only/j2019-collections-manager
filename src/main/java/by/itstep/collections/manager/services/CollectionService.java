package by.itstep.collections.manager.services;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;

import java.util.List;

public interface CollectionService {
    List<CollectionPreviewDto> findAll();

    CollectionFullDto findById(Long id);

    CollectionFullDto create(CollectionCreateDto createDto);

   CollectionFullDto update(CollectionUpdateDto updateDto);

    void deleteById(Long id);
}
