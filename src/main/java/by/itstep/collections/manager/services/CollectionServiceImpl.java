package by.itstep.collections.manager.services;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;

import java.util.List;

public class CollectionServiceImpl implements CollectionService {

    private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
    private CollectionMapper mapper = new CollectionMapper();
    private UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + " collections");
        return mapper.mapToDtoList(found);

    }

    @Override
    public CollectionFullDto findById(Long id) {
        Collection found = collectionRepository.findById(id);
        System.out.println("CollectionServiceImpl -> found collection: " + found);
        return mapper.mapToDto(found);
    }

    @Override
    public CollectionFullDto create(CollectionCreateDto createDto) {
        User user =userRepository.findById(createDto.getUserId());
        Collection toSave = mapper.mapToEntity(createDto, user);
        Collection created = collectionRepository.create(toSave);
        System.out.println("CollectionServiceImpl -> created collection: " + created);

        return mapper.mapToDto(created);
    }

    @Override
    public CollectionFullDto update(CollectionUpdateDto updateDto) {
        //User user = userRepository.findById(updateDto.getUserId());
        Collection entityToUpdate = collectionRepository.update(mapper.mapToEntity(updateDto));
        Collection existingEntity = collectionRepository.findById(entityToUpdate.getId());
        entityToUpdate.setUser(existingEntity.getUser());
        Collection updated = collectionRepository.update(entityToUpdate);
        System.out.println("ColectionServiceImpl -> updated updateDto: " + entityToUpdate);
        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        collectionRepository.deleteById(id);
        System.out.println("CollectionServiceImpl -> collection with id " + id + " was deleted");
    }
}
