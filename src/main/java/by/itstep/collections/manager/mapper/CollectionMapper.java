package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CollectionMapper {
   public List<CollectionPreviewDto> mapToDtoList(List<Collection> entities){
         List<CollectionPreviewDto> dtos = new ArrayList<>();
       for (Collection entity:
            entities) {
           CollectionPreviewDto dto = new CollectionPreviewDto();
           dto.setId(entity.getId());
           dto.setName(entity.getName());
           dto.setTitle(entity.getTitle());
           dto.setImageUrl(entity.getImageUrl());
           dto.setTags(entity.getTags());
           dto.setUserName(entity.getUser().getName() + " " + entity.getUser().getLastName());
            dtos.add(dto);
       }
         return dtos;
    }

    public Collection mapToEntity(CollectionCreateDto createDto, User user){
       Collection entity = new Collection();
       entity.setName(createDto.getName());
       entity.setTitle(createDto.getTitle());
       entity.setDiscription(createDto.getDiscription());
       entity.setImageUrl(createDto.getImageUrl());
       entity.setUser(user);
       return entity;
    }

    public Collection mapToEntity(CollectionUpdateDto updateDto){
        Collection entity = new Collection();
        entity.setId(updateDto.getId());
        entity.setName(updateDto.getName());
        entity.setTitle(updateDto.getTitle());
        entity.setDiscription(updateDto.getDiscription());
        entity.setImageUrl(updateDto.getImageUrl());
        //collection.setUser(user);
        return entity;
    }

    public CollectionFullDto mapToDto(Collection collection){
        CollectionFullDto fullDto = new CollectionFullDto();
        fullDto.setId(collection.getId());
        fullDto.setName(collection.getName());
        fullDto.setTitle(collection.getTitle());
        fullDto.setImageUrl(collection.getImageUrl());
        fullDto.setTags(collection.getTags());
        fullDto.setUserName(collection.getUser().getName() + " " + collection.getUser().getLastName());
        fullDto.setComents(collection.getComments());
        fullDto.setItems(collection.getItems());
        fullDto.setDiscription(collection.getDiscription());
        return fullDto;
    }
}
