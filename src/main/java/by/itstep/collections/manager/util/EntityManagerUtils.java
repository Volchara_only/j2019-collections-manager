package by.itstep.collections.manager.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtils {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY =
            Persistence.createEntityManagerFactory("collection-unit");

    public static EntityManager getEntityManger(){
        return ENTITY_MANAGER_FACTORY.createEntityManager();
    }
    
}
