package by.itstep.collections.manager.entity;
 import lombok.*;

 import javax.persistence.*;
 import java.sql.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="message", nullable = false)
    private String message;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name="collection_id")
    private Collection  collection;
    @Column(name="created_at")
    private Date createdAt;
}
