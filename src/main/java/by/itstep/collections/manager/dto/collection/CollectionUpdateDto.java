package by.itstep.collections.manager.dto.collection;

import by.itstep.collections.manager.entity.Tag;
import lombok.Data;

import java.util.List;
@Data
public class CollectionUpdateDto {
    private Long id;
    private String name;
    private String title;
    private String discription;
    private String imageUrl;
    //private Long userId;
    private List<Tag> tags;
}
