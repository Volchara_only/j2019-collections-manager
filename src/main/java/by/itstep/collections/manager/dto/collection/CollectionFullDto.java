package by.itstep.collections.manager.dto.collection;

import by.itstep.collections.manager.entity.CollectionItems;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.Tag;
import lombok.Data;

import java.util.List;
@Data
public class CollectionFullDto {
    private Long id;
    private String name;
    private String title;
    private String imageUrl;
    private String userName;
    private String discription;
    private List<Tag> tags;
    private List<CollectionItems> items;
    private List<Comment> coments;
}
