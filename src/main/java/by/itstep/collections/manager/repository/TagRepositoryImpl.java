package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class TagRepositoryImpl implements TagRepository{
    @Override
    public List<Tag> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        List<Tag> foundList = em.createNativeQuery("SELECT * FROM tag",
                Tag.class).getResultList();
        em.close();
        return foundList;
    }

    @Override
    public Tag findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        Tag foundTag = em.find(Tag.class,id);
        em.close();
        return foundTag;
    }

    @Override
    public Tag create(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(tag);
        em.getTransaction().commit();
        em.close();
        return tag;
    }

    @Override
    public Tag update(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(tag);
        em.getTransaction().commit();
        em.close();
        return tag;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        Tag foundTag = em.find(Tag.class,id);
        em.remove(foundTag);
        em.getTransaction().commit();
        em.close();
    }
}
