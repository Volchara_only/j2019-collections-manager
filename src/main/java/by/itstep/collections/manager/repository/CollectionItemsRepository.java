package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItems;

import java.util.List;

public interface CollectionItemsRepository {
    List<CollectionItems> findAll();
    CollectionItems findById(Long id);
    CollectionItems create(CollectionItems collectionItems);
    CollectionItems update(CollectionItems collectionItems);
    void deleteById(Long id);
    void deleteAll();
}
