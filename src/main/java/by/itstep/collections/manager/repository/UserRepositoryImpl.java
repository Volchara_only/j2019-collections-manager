package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryImpl implements UserRepository{
    @Override
    public List<User> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        List<User> foundList = em.createNativeQuery("SELECT * FROM user",
                User.class).getResultList();
        em.close();
        return foundList;
    }

    @Override
    public User findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        User foundUser = em.find(User.class, id);
        em.close();
        return foundUser;
    }

    @Override
    public User create(User user) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    @Override
    public User update(User user) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        User foundUser = em.find(User.class, id);
        em.remove(foundUser);
        em.getTransaction().commit();
        em.close();
    }
}
