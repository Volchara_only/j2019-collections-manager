package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class CollectionRepositoryImpl implements CollectionRepository {
    @Override
    public List<Collection> findAll() {
        EntityManager em=EntityManagerUtils.getEntityManger();
        List<Collection> foundList = em.createNativeQuery("SELECT * FROM" +
                                     " collection", Collection.class).getResultList();
        em.close();
        return foundList;
    }

    @Override
    public Collection findById(Long id) {
        EntityManager em=EntityManagerUtils.getEntityManger();
        Collection foundCollection = em.find(Collection.class, id);
        Hibernate.initialize(foundCollection.getItems());
        em.close();
        return foundCollection;
    }

    @Override
    public Collection create(Collection collection) {
        EntityManager em=EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(collection);
        em.getTransaction().commit();
        em.close();
        return collection;
    }

    @Override
    public Collection update(Collection collection) {
        EntityManager em=EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(collection);
        em.getTransaction().commit();
        em.close();
        return collection;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em=EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        Collection foundCollection = em.find(Collection.class, id);
        em.remove(foundCollection);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM collection").executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
