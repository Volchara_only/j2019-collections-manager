package by.itstep.collections.manager.repository;


import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository{
    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        List<Comment> foundList = em.createNativeQuery("SELECT * FROM comment",
                Comment.class).getResultList();
        em.close();
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        Comment foundComment = em.find(Comment.class, id);
        em.close();
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(comment);
        em.getTransaction().commit();
        em.close();
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.merge(comment);
        em.getTransaction().commit();
        em.close();
        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        Comment foundComment = em.find(Comment.class, id);
        em.remove(foundComment);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM comment").executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
