package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItems;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CollectionItemsRepositoryImpl implements CollectionItemsRepository{
    @Override
    public List<CollectionItems> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        List<CollectionItems> foundList = em.createNativeQuery("SELECT * FROM collection_items",
                CollectionItems.class).getResultList();
        em.close();
        return foundList;
    }

    @Override
    public CollectionItems findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        CollectionItems foundCollectionItems = em.find(CollectionItems.class, id);
        em.close();
        return foundCollectionItems;
    }

    @Override
    public CollectionItems create(CollectionItems collectionItems) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(collectionItems);
        em.getTransaction().commit();
        em.close();
        return collectionItems;
    }

    @Override
    public CollectionItems update(CollectionItems collectionItems) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.persist(collectionItems);
        em.getTransaction().commit();
        em.close();
        return collectionItems;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        CollectionItems foundCollectionItems = em.find(CollectionItems.class, id);
        em.remove(foundCollectionItems);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManger();
        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM collection_items").executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
