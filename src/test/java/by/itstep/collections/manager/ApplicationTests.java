package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItems;
import by.itstep.collections.manager.repository.CollectionItemsRepository;
import by.itstep.collections.manager.repository.CollectionItemsRepositoryImpl;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ApplicationTests {
     private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
     private CollectionItemsRepository itemsRepository = new CollectionItemsRepositoryImpl();
     @BeforeEach
	 public void setUp(){
     	itemsRepository.deleteAll();
     	collectionRepository.deleteAll();
	 }
	@Test
	void saveCollectionWithoutItems() {
		Collection collection = Collection.builder()
				.name("my_name")
				.discription("my-discripton")
				.imageUrl("my_URL")
				.title("my_title")
				.build();
		Collection saved = collectionRepository.create(collection);
		Assertions.assertNotNull(saved.getId());
	}
	@Test
	void testFindAll() {
		Collection collection1 = Collection.builder()
				.name("my_name")
				.discription("my-discripton")
				.imageUrl("my_URL")
				.title("my_title")
				.build();
		Collection collection2 = Collection.builder()
				.name("my_name")
				.discription("my-discripton")
				.imageUrl("my_URL")
				.title("my_title")
				.build();
		collectionRepository.create(collection1);
		collectionRepository.create(collection2);
		List<Collection> found = collectionRepository.findAll();
		Assertions.assertEquals(2,found.size());
	}
	@Test
	void saveCollectionWithItems() {
		Collection collection = Collection.builder()
				.name("my_name")
				.discription("my-discripton")
				.imageUrl("my_URL")
				.title("my_title")
				.build();
		Collection saved = collectionRepository.create(collection);
		CollectionItems i1 = CollectionItems.builder().name("item1").collection(saved).build();
		CollectionItems i2 = CollectionItems.builder().name("item2").collection(saved).build();
		CollectionItems savedI1 = itemsRepository.create(i1);
		CollectionItems savedI2 = itemsRepository.create(i2);
		List<CollectionItems> items = new ArrayList<>();
		items.add(savedI1);
		items.add(savedI2);



		// then
		Assertions.assertNotNull(saved.getId());
	}
	@Test
	void Test4() {
	}
}
